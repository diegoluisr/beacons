package co.mocion.fonts;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dierestr1 on 1/21/17.
 */

public class FontSingleton {
    private static FontSingleton selfInstance = null;

    protected static Map<String, Typeface> fonts;

    protected FontSingleton() {}

    public static synchronized FontSingleton getInstance(){
        fonts = new HashMap<>();
        if(null == selfInstance){
            selfInstance = new FontSingleton();
        }
        return selfInstance;
    }

    public static synchronized void addFontSupport(AssetManager manager, String family, String path){
        Typeface font = Typeface.createFromAsset(manager, path);
        fonts.put(family, font);
    }

    public static synchronized Typeface getFont(String name){
        for (Map.Entry<String, Typeface> font : fonts.entrySet()) {
            if(font.getKey() == name) {
                return font.getValue();
            }
        }
        return null;
    }
}

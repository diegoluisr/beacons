package com.prodigious.renault.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.prodigious.renault.AppProperties;
import com.prodigious.renault.BaseApplication;
import com.prodigious.renault.R;

import co.mocion.fonts.FontSingleton;

public class BeaconBlueActivity extends AppCompatActivity {

    protected Button acceptButton;
    protected TextView line0, line2, zero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_beacon_blue);

        acceptButton = (Button) findViewById(R.id.accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppProperties.getInstance().points += 10;
                SharedPreferences settings = getSharedPreferences(BaseApplication.PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("points", AppProperties.getInstance().points);
                editor.apply();

                Intent recoverIntent = new Intent().setClass(getBaseContext(), MainActivity.class);
                startActivity(recoverIntent);
                finish();
            }
        });
        acceptButton.setTypeface(FontSingleton.getFont("Univers"));

        line0 = (TextView) findViewById(R.id.line0);
        line0.setTypeface(FontSingleton.getFont("RenaultLife"));

        line2 = (TextView) findViewById(R.id.line2);
        line2.setTypeface(FontSingleton.getFont("RenaultLife"));

        zero = (TextView) findViewById(R.id.zero);
        zero.setTypeface(FontSingleton.getFont("RenaultLifeBold"));

        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
}

package com.prodigious.renault.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.prodigious.renault.R;

import co.mocion.fonts.FontSingleton;

public class BeaconIceActivity extends AppCompatActivity {

    protected Button acceptButton;
    protected TextView line0, line2;
    protected ImageView zero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_beacon_ice);

        acceptButton = (Button) findViewById(R.id.accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent recoverIntent = new Intent().setClass(getBaseContext(), MainActivity.class);
                startActivity(recoverIntent);
                finish();
            }
        });
        acceptButton.setTypeface(FontSingleton.getFont("Univers"));

        line0 = (TextView) findViewById(R.id.line0);
        line0.setTypeface(FontSingleton.getFont("RenaultLife"));

        line2 = (TextView) findViewById(R.id.line2);
        line2.setTypeface(FontSingleton.getFont("RenaultLife"));

        zero = (ImageView) findViewById(R.id.zero);
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://maps.google.com/maps?daddr=4.681305,-74.116915&saddr=";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
}

package com.prodigious.renault.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;

import com.prodigious.renault.R;

import co.mocion.fonts.FontSingleton;

public class WebActivity extends AppCompatActivity {

    protected Button acceptButton;
    protected WebView webviewWebview;

    protected String previousActivity;
    protected String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_web);

        Bundle extra = getIntent().getExtras();
        previousActivity = extra.getString("PreviousActivity");
        url = extra.getString("URL");

        acceptButton = (Button) findViewById(R.id.accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(previousActivity == null) {
                    Intent recoverIntent = new Intent().setClass(getBaseContext(), MainActivity.class);
                    startActivity(recoverIntent);
                }
                finish();
            }
        });
        acceptButton.setTypeface(FontSingleton.getFont("Univers"));

        webviewWebview = (WebView) findViewById(R.id.webview);

        if(url != null) {
            webviewWebview.loadUrl(url);
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
}

package com.prodigious.renault.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.estimote.sdk.EstimoteSDK;
import com.estimote.sdk.SystemRequirementsChecker;
import com.prodigious.renault.AppProperties;
import com.prodigious.renault.BaseApplication;
import com.prodigious.renault.R;

import java.util.Timer;
import java.util.TimerTask;

import co.mocion.fonts.FontSingleton;

public class MainActivity extends AppCompatActivity {

    private static final long MAIN_SCREEN_DELAY = 3500;

    private TextView hi;
    private TextView name;
    private TextView haves;
    private TextView points;
    private TextView search;
    private TextView zero;

    private RelativeLayout hero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        hi = (TextView) findViewById(R.id.hi);
        name = (TextView) findViewById(R.id.name);
        haves = (TextView) findViewById(R.id.haves);
        points = (TextView) findViewById(R.id.points);
        search = (TextView) findViewById(R.id.search);
        zero = (TextView) findViewById(R.id.zero);

        hi.setTypeface(FontSingleton.getFont("RenaultLife"));
        name.setTypeface(FontSingleton.getFont("RenaultLife"));
        haves.setTypeface(FontSingleton.getFont("RenaultLife"));
        points.setTypeface(FontSingleton.getFont("RenaultLifeBold"));
        search.setTypeface(FontSingleton.getFont("RenaultLife"));
        zero.setTypeface(FontSingleton.getFont("RenaultLifeBold"));

        search.setText(Html.fromHtml(getString(R.string.msg_html_main_cuota)));

        hero = (RelativeLayout) findViewById(R.id.hero);

        hero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent remote = new Intent().setClass(MainActivity.this, RemoteActivity.class);
                startActivity(remote);
            }
        });


        // EstimoteSDK.initialize(this.getApplicationContext(), getString(R.string.estimote_app_id), getString(R.string.estimote_app_token));

        if(!AppProperties.getInstance().ready) {
            AppProperties.getInstance().ready = true;

            SharedPreferences settings = getSharedPreferences(BaseApplication.PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("ready", true);
            editor.apply();

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    Intent screenIntent = new Intent().setClass(
                            MainActivity.this, InviteActivity.class);
                    screenIntent.putExtra("PreviousActivity", "MainActivity");
                    startActivity(screenIntent);
                    // finish();
                }
            };

            Timer timer = new Timer();
            timer.schedule(task, MAIN_SCREEN_DELAY);

            // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        points.setText(AppProperties.getInstance().points + " pts");
    }

    @Override
    protected void onResume() {
        super.onResume();

        SystemRequirementsChecker.checkWithDefaultDialogs(this);
    }
}

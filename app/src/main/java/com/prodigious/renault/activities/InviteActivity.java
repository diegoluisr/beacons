package com.prodigious.renault.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.prodigious.renault.R;

import co.mocion.fonts.FontSingleton;
import cz.msebera.android.httpclient.Header;

public class InviteActivity extends AppCompatActivity {

    protected Button acceptButton;
    protected TextView line0, line1, line2;
    protected TextView zero, symbol;

    protected String previousActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_invite);

        Bundle extra = getIntent().getExtras();
        previousActivity = extra.getString("PreviousActivity");

        acceptButton = (Button) findViewById(R.id.accept);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openExternalScreen();
            }
        });
        acceptButton.setTypeface(FontSingleton.getFont("Univers"));

        line0 = (TextView) findViewById(R.id.line0);
        line0.setTypeface(FontSingleton.getFont("RenaultLife"));

        line1 = (TextView) findViewById(R.id.line1);
        line1.setTypeface(FontSingleton.getFont("RenaultLife"));

        line2 = (TextView) findViewById(R.id.line2);
        line2.setTypeface(FontSingleton.getFont("RenaultLife"));

        zero = (TextView) findViewById(R.id.zero);
        zero.setTypeface(FontSingleton.getFont("RenaultLifeBold"));

        symbol = (TextView) findViewById(R.id.symbol);
        symbol.setTypeface(FontSingleton.getFont("RenaultLifeBold"));

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    protected void openExternalScreen() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://104.236.11.183/renault-image", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                acceptButton.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                closeActivity();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                closeActivity();
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                closeActivity();
            }
        });
    }

    protected void closeActivity() {
        if(previousActivity == null) {
            Intent recoverIntent = new Intent().setClass(getBaseContext(), MainActivity.class);
            startActivity(recoverIntent);
        }
        finish();
    }
}

package com.prodigious.renault;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.prodigious.renault.activities.MainActivity;
import com.prodigious.renault.services.BeaconMonitoringIntentService;
import com.prodigious.renault.services.BeaconMonitoringService;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import co.mocion.fonts.FontSingleton;

/**
 * Created by diegoluisr on 1/14/17.
 */

public class BaseApplication extends Application {

    public static final String PREFS_NAME = "LocalPrefsFile";

    public static final String FONT_RENAULT_LIFE_REGULAR = "fonts/RenaultLife-Regular.ttf";
    public static final String FONT_RENAULT_LIFE_BOLD = "fonts/RenaultLife-Bold.ttf";
    public static final String FONT_UNIVERS = "fonts/Univers-CondensedOblique.otf";

    private FontSingleton fonts;

    @Override
    public void onCreate() {
        super.onCreate();

        fonts = FontSingleton.getInstance();

        fonts.addFontSupport(getAssets(), "RenaultLife", FONT_RENAULT_LIFE_REGULAR);
        fonts.addFontSupport(getAssets(), "RenaultLifeBold", FONT_RENAULT_LIFE_BOLD);
        fonts.addFontSupport(getAssets(), "Univers", FONT_UNIVERS);

        initSingletons();

//        startService(new Intent(this, BeaconMonitoringService.class));

    }

    protected void initSingletons() {

        SharedPreferences settings = getSharedPreferences(BaseApplication.PREFS_NAME, 0);
        boolean ready = settings.getBoolean("ready", false);
        int points = settings.getInt("points", 2600);

        AppProperties.getInstance().ready = ready;
        AppProperties.getInstance().points = points;
    }

    public void savePoints(){
        SharedPreferences settings = getSharedPreferences(BaseApplication.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("points", AppProperties.getInstance().points);
        editor.apply();
    }
}

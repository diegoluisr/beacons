package com.prodigious.renault.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.prodigious.renault.activities.WebActivity;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class BeaconMonitoringIntentService extends IntentService {

    private BeaconManager beaconManager;

    private boolean mintReady, blueReady, iceReady = false;

    public BeaconMonitoringIntentService() {
        super("BeaconMonitoringIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();

            beaconManager = new BeaconManager(this);
            beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                @Override
                public void onServiceReady() {
                    beaconManager.setBackgroundScanPeriod(TimeUnit.SECONDS.toMillis(1), 0);
                    beaconManager.startMonitoring(new Region("prodigious", UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null));
                }
            });
            beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
                @Override
                public void onEnteredRegion(Region region, List<Beacon> beacons) {
                    for (Iterator<Beacon> item = beacons.iterator(); item.hasNext();) {
                        Beacon beacon = item.next();
                        Log.i("Beacon: ", beacon.getProximityUUID().toString());

                        switch(beacon.getMinor()) {
                            case 22305:
                                showNotification("Renault en Publicis One", "Llegaste a la cuenta de Helios", "http://mocion.co/renault/ice.html");
                                break;
                            case 52216:
                                showNotification("Renault en Publicis One", "¡Encontraste mas ceros!", "http://mocion.co/renault/blue.html");
                                break;
                            case 39848:
                                showNotification("Renault en Publicis One", "¿Conoces nuestra nueva pickup?", "http://mocion.co/renault/mint.html");
                                break;
                        }
                    }
                    if(mintReady && blueReady && iceReady) {
                        stopSelf();
                    }
                }
                @Override
                public void onExitedRegion(Region region) {
                    // could add an "exit" notification too if you want (-:
                }
            });
        }
    }

    public void showNotification(String title, String message, String url) {
        Intent notifyIntent = new Intent(this, WebActivity.class);
        notifyIntent.putExtra("URL", url);

        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[] { notifyIntent }, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }
}

package com.prodigious.renault.services;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.prodigious.renault.activities.BeaconBlueActivity;
import com.prodigious.renault.activities.BeaconIceActivity;
import com.prodigious.renault.activities.BeaconMintActivity;
import com.prodigious.renault.activities.MainActivity;
import com.prodigious.renault.activities.WebActivity;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BeaconMonitoringService extends Service {

    private BeaconManager beaconManager;

    private BeaconMonitoringService bms;

    private Region region;
    private Region regionMint;
    private Region regionBlue;
    private Region regionIce;

    private boolean mintReady, blueReady, iceReady = false;

    // private String user;

    public BeaconMonitoringService() {
        // Log.d("This", this.getApplicationContext().toString());
        // beaconManager = new BeaconManager(this);
        bms = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // android.os.Debug.waitForDebugger();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        // region = new Region("prodigious", UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);

        regionMint = new Region("prodigious", UUID.fromString("708FE47A-2BD5-9EC4-31B0-419F0569DBFA"), null, null);
        regionBlue = new Region("prodigious", UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);
        regionIce = new Region("prodigious", UUID.fromString("5BCA9DBD-F2CD-E9F8-7B77-24F9FCD75866"), null, null);

        beaconManager = new BeaconManager(this);
        beaconManager.setBackgroundScanPeriod(TimeUnit.SECONDS.toMillis(2), TimeUnit.SECONDS.toMillis(1));
        beaconManager.setRegionExitExpiration(TimeUnit.SECONDS.toMillis(5));
        // beaconManager.setForegroundScanPeriod(TimeUnit.SECONDS.toMillis(2), 0);

        Toast.makeText(this, "Beacons monitoring service starting", Toast.LENGTH_SHORT).show();

        // user = intent.getStringExtra("user");

        connectToService();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Beacons monitoring service done", Toast.LENGTH_SHORT).show();
        // beaconManager.stopRanging(region);
    }

    private void connectToService() {

        beaconManager.setScanStatusListener(new BeaconManager.ScanStatusListener() {
            @Override
            public void onScanStart() {
                Toast.makeText(bms, "Beacon status started", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onScanStop() {
                Toast.makeText(bms, "Beacon status stoped", Toast.LENGTH_SHORT).show();
            }
        });

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startMonitoring(regionMint);
                beaconManager.startMonitoring(regionBlue);
                beaconManager.startMonitoring(regionIce);
                // beaconManager.setForegroundScanPeriod(TimeUnit.SECONDS.toMillis(3600), 0);
                // beaconManager.startRanging(region);
            }
        });

        /* beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {
                Log.e("Beacons discovered", "OK");
                if(!beacons.isEmpty()){
                    for (Iterator<Beacon> item = beacons.iterator(); item.hasNext();) {
                        Beacon beacon = item.next();
                        Log.i("Beacon: ", beacon.getProximityUUID().toString());

                        switch(beacon.getMinor()) {
                            case 22305:
                                // showNotification("Renault en Publicis One", "Llegaste a la cuenta de Helios", "http://mocion.co/renault/ice.html");
                                break;
                            case 52216:
                                mintReady = true;
                                showNotification("Renault / Publicis One", "Marcelo, bienvenido al grupo de trabajo de Helios. Mira cuantos 0 tienes acumulados.", BeaconBlueActivity.class);
                                break;
                            case 39848:
                                // showNotification("Renault en Publicis One", "¿Conoces nuestra nueva pickup?", "http://mocion.co/renault/mint.html");
                                break;
                        }
                    }
                    if(mintReady ) { // && blueReady && iceReady
                        stopSelf();
                    }
                }
            }
        });*/

        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> beacons) {
                for (Iterator<Beacon> item = beacons.iterator(); item.hasNext();) {
                    Beacon beacon = item.next();
                    Log.i("Beacon: ", beacon.getProximityUUID().toString());

                    switch(beacon.getMinor()) {
                        case 22305:
                            if(!iceReady) {
                                iceReady = true;
                                showNotification("Renault / Publicis One", "Marcelo, te encuentras a 7 metros del grupo de trabajo de Publicis One. Ver mapa.", BeaconIceActivity.class);
                            }
                            break;
                        case 52216:
                            if(!blueReady) {
                                blueReady = true;
                                showNotification("Renault / Publicis One", "Marcelo, bienvenido al grupo de trabajo de Helios. Mira cuantos 0 tienes acumulados.", BeaconBlueActivity.class);
                            }
                            break;
                        case 39848:
                            if(!mintReady) {
                                mintReady = true;
                                showNotification("Renault / Publicis One", "Marcelo acabas de llegar al segundo piso de Prodigious. Mira cuantos 0 tienes acumulados.", BeaconMintActivity.class);
                            }
                            break;
                    }
                }
                if(mintReady && blueReady && iceReady) {
                    beaconManager.stopMonitoring(regionIce);
                    beaconManager.stopMonitoring(regionBlue);
                    beaconManager.stopMonitoring(regionMint);
                    stopSelf();
                }
            }
            @Override
            public void onExitedRegion(Region region) {
                // could add an "exit" notification too if you want (-:
            }
        });
    }

    public void showNotification(String title, String message, Class className) {
        Intent notifyIntent = new Intent(this, className);
        // notifyIntent.putExtra("URL", url);

        int rand = (int)(Math.random() * 1000000);

        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[] { notifyIntent }, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(rand, notification);
    }
}

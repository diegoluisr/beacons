package com.prodigious.renault;

/**
 * Created by dierestr1 on 1/24/17.
 */
public class AppProperties {

    private static AppProperties ourInstance = new AppProperties();

    public static boolean ready = false;
    public static int points = 2600;

    public static synchronized AppProperties getInstance() {

        if(null == ourInstance){
            ourInstance = new AppProperties();
        }
        return ourInstance;
    }

    private AppProperties() {
    }

}
